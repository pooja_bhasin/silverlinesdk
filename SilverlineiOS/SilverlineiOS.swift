//
//  SilverlineiOS.swift
//  SilverlineiOS
//
//  Created by Sonali Rana on 6/24/19.
//  Copyright © 2019 Sonali Rana. All rights reserved.
//

import UIKit


open class SilverlineiOS: NSObject {
    
    let AESKey = "SL_AESKey"
    let auth = "SL_Auth"
    public static let shared = SilverlineiOS()
    public var customerId: String?
    public var applicationid: String?
    public var environment: String?
    public var baseUrl: String?
    public var azureUrl = String()
    public var appKey = String()
    var publicKey = String()
    public var privatekey = String()
    var userName = String()
    var userPassword = String()
    var responseDict = [String: AnyObject]()
    
    public typealias successCallback = (_ response: Any?, _ isSucess: Bool, _ message: String) -> Void
    
    public func makeCallToSilverline(username: String, password: String, callback: @escaping successCallback) {
        self.userName = username
        self.userPassword = password
        let params = ["customerID": customerId, "appID": applicationid, "env": environment]
        APIManager.sharedInstance.serverCallForLoginMethod(baseUrl! + API.requestURL, params: params as [String : Any]) { (result, isSuccess, error) in
            if isSuccess {
                if let appKeyData = result as [String: AnyObject]? {
                    print(appKeyData)
                    if let uri = appKeyData["uri"]as? String {
                        self.azureUrl = uri
                    }
                    if let appKeyVal = appKeyData["appKey"]as? String {
                        self.appKey = appKeyVal
                    }
                    print("Login First Call >>>>>>> \(appKeyData)")
                    self.apiKeyForPublicKey(callback: { (result, isSuccess, message) in
                        if isSuccess {
                            self.apiCallForPrivateKey(callback: { (result, isSuccess, message) in
                                if isSuccess {
                                    self.loginApiCall(callback: { (result, isSuccess, message) in
                                        if isSuccess {
                                            if let response = result as! [String: AnyObject]? {
                                                print("Inside First Function \(response)")
                                                callback(response,true,error)
                                            }
                                        } else {
                                            callback(result,false,error)
                                        }
                                    })
                                } else {
                                    callback(result,false,error)
                                }
                            })
                        } else {
                            callback(result,false,error)
                        }
                    })
                }
            } else {
                callback(result,false,error)
            }
        }
    }
    
    
    
    func apiKeyForPublicKey(callback: @escaping successCallback) {
        let msgbodypub = CommonFunction.sharedInstance.aesKeyMessageBody(keyType: "AES_PUBLIC_KEY")
        let apiUrl = azureUrl + "/API" + "/" +  AESKey
        APIManager.sharedInstance.serverCallForLoginMethod(apiUrl, params: msgbodypub) { (result, isSuccess, error) in
            if isSuccess {
                if let response = result as [String: AnyObject]? {
                    if let requestbody = response["respBody"] as? [ String : AnyObject] {
                        if let publicKeyVal = requestbody["key"]as? String {
                            self.publicKey = publicKeyVal
                            print("Login publicKey >>>>>>> \(publicKeyVal)")
                        }
                    }
                    callback(response,true,error)
                } else {
                    callback(result,false,error)
                }
            }
        }
    }
        
        func apiCallForPrivateKey(callback: @escaping successCallback) {
            let msgbodyprivate = CommonFunction.sharedInstance.aesKeyMessageBody(keyType: "AES_PRIVATE_KEY")
            let apiUrl = azureUrl + "/API" + "/" +  AESKey
            
            APIManager.sharedInstance.serverCallForLoginMethod(apiUrl, params: msgbodyprivate) { (result, isSuccess, error) in
                if isSuccess {
                    if let response = result as [String: AnyObject]? {
                        if let requestbody = response["respBody"] as? [ String : AnyObject] {
                            if let privateKey = requestbody["key"]as? String {
                                if let privateEncrKey = CommonFunction.sharedInstance.decryptMessage(encryptedMessage: privateKey, encryptionKey: self.publicKey)as? String  {
                                    self.privatekey = privateEncrKey
                                    print("Login privatekey >>>>>>> \(privateEncrKey)")
                                }
                            }
                        }
                        callback(result,true,error)
                    }
                } else {
                    callback(result,false,error)
                }
            }
        }
        
        
        func loginApiCall(callback: @escaping successCallback)  {
            let pushtag = NSUUID().uuidString as AnyObject
            let payload:String  = CommonFunction.sharedInstance.encryptMessage(message: CommonFunction.sharedInstance.createAuthPayload(self.userPassword, pushtag: pushtag as! String), encryptionKey: privatekey)
            let requestBody = CommonFunction.sharedInstance.createAuthMsgBody(payload: payload, username: self.userName, custID: customerId!, appID: applicationid!)
            let apiUrl = azureUrl + "/API" + "/" +  auth
            APIManager.sharedInstance.serverCallForLoginMethod(apiUrl, params: requestBody) { (result, isSuccess, error) in
                if isSuccess {
                    callback(result,true,error)
                } else {
                    callback(result,false,error)
                }
            }
        }
        
}
