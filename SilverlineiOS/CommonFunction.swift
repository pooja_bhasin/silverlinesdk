//
//  CommonFunction.swift
//  SilverlineiOS
//
//  Created by Sonali Rana on 6/24/19.
//  Copyright © 2019 Sonali Rana. All rights reserved.
//

import Foundation
import CommonCrypto
import CRDCrypt

open class CommonFunction: NSObject {
    
    public static let sharedInstance = CommonFunction()
    
   open func aesKeyMessageBody(keyType:String) -> [String: AnyObject] {
        let authDateFormatter = DateFormatter()
        authDateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        authDateFormatter.dateFormat = "yyyyMMddHHmmssSSS"
        var messageBody = [String: AnyObject]()
        let messageRequestHeader = ["appID": SilverlineiOS.shared.applicationid, "customerID": SilverlineiOS.shared.customerId, "TxType": "SLAES", "messageId":String (format : "AES#%@#%@#%@",SilverlineiOS.shared.applicationid!,"SLAES",authDateFormatter .string(from: Date()).uppercased()),"timestamp":authDateFormatter .string(from: Date())]
        let messageRespBody = ["keyType":keyType]
        messageBody["reqHeader"] = messageRequestHeader as AnyObject
        messageBody["reqBody"] = messageRespBody as AnyObject
        return messageBody
    }
    
   open func decryptMessage(encryptedMessage: String, encryptionKey: String) -> String {
        let decodedData = Data(base64Encoded: encryptedMessage, options: [])
        var decryptedString: String? = nil
        do {
            decryptedString = String(bytes: try decodedData!.aes256Decrypt(withKey: encryptionKey, initializationVector: nil), encoding: .utf8)
        } catch let error as NSError {
            print("\(error)")
        }
        return decryptedString!
    }
    
   open func encryptMessage(message: String, encryptionKey: String) -> String {
        var encryptedString: String? = nil
        var encryptedData: Data? = nil
        do
        {
            encryptedData = try! message.data(using: .utf8)?.aes256Encrypt(withKey: encryptionKey, initializationVector: nil)
        }
        encryptedString = encryptedData!.base64EncodedString()
        return encryptedString!
    }
    
   open func createAuthPayload(_ passcode: String, pushtag: String) -> String {
        var jsonString = ""
        let pushtag = "\(NSUUID().uuidString)"
        let deviceId: String = (UIDevice.current.identifierForVendor?.uuidString)!
        let payload = ["Password": passcode, "AuthToken": "", "Tag": pushtag, "DeviceId": deviceId , "OSVer": UIDevice.current.systemVersion,"OS":"IOS"] as [String: Any]
        print(payload)
        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: payload, options:[])
        if !(jsonData != nil) {
        }
        else {
            jsonString = String(data: jsonData!, encoding: String.Encoding.utf8)!
        }
        return jsonString
    }
    
    
   open func createAuthMsgBody(payload: String, username: String, custID: String, appID:String) -> [String :[String:String]] {
        let TxType = "EMSAUTHA"
        let LanguageCode = Locale.current.languageCode
        let authDateFormatter = DateFormatter()
        authDateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
        authDateFormatter.dateFormat = "yyyyMMddHHmmssSSS"
        var messageBody = [String :[String:String]]()
        let requestHeader = ["username":username, "appID":appID, "langCode":LanguageCode, "customerID":custID ,"messageId":String (format : "%@#%@#%@#%@",username,appID,TxType,authDateFormatter .string(from: Date()).uppercased()),"txType":TxType, "timestamp":authDateFormatter .string(from: Date()) ,"encrypted":"True" ] as! [String : String]
        var requestBody = [String : String]()
        requestBody["payload"] = payload
        messageBody["reqHeader"] = requestHeader
        messageBody["reqBody"] = requestBody
        return messageBody
    }

}
