//
//  APIManager.swift
//  EMSAttendee
//
//  Created by Sonali Rana on 7/26/18.
//  Copyright © 2018 Pooja Bhasin. All rights reserved.
//

import UIKit
import SystemConfiguration
import Alamofire


struct API {
    static let requestURL = "/api/SL_MobileService"

}

public class APIManager {
    
    static let sharedInstance = APIManager()
    var data = NSMutableData()
     
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    

    func getJsonHeaderWithZumo() -> [String:String]{
        return ["Content-Type":"application/json", "ZUMO-API-VERSION":"2.0.0"]
    }
    
    func serverCallForLoginMethod(_ apiName: String, params : [String : Any],_ completion: @escaping ( _ response: [String:Any]?,  _ success : Bool,  _ error : String) -> Void) {
        if !APIManager.isConnectedToNetwork() {
            //APIManager().networkErrorMsg()
            completion(nil, false, "Error")

            return
        }
        //showLoader()
        let headerParams :[String : String] = getJsonHeaderWithZumo()
        Alamofire.request(apiName, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headerParams).responseJSON { (response) in
            //removeLoader()
            switch response.result {
            case .success:
                print(response.result.value!)
                if let result = response.result.value as? [String:Any]{
                    completion(result, true, "")
                }
                if let error = response.result.error {
                    completion(nil, false, error.localizedDescription)
                }
                break
            case .failure(let error):
                print(error)
                completion(nil, false, error.localizedDescription)
                //displayToast(error.localizedDescription)
                break
            }
        }
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: URLResponse!) {
        print("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection!, didReceiveData conData: NSData!) {
        self.data.append(conData as Data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        print(self.data)
    }
    
    
    deinit {
        print("deiniting")
    }
    
  
    
}


